package nontio.addapp.com.fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import nontio.addapp.com.tv.R;

/**
 * Created by test on 1/20/2015.
 */
public class TickerFragment extends Fragment {

    ArrayList<String> list = new ArrayList<String>();
    TextView counter, tick;
    AutoResizeTextView ticker;
    int count = 0, arraySize = 0;

    String dummy1 = "What is Lorem Ipsum\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
    String dummy2 = "Why do we use it\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.";
    String dummy3 = "Where it comes from\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.";
    String dummy4 = "Where can I get some\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.ticker_view, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        tick = (TextView)getView().findViewById(R.id.ticker_text);
        ticker = (AutoResizeTextView)getView().findViewById(R.id.ticker_text);
        counter = (TextView) getView().findViewById(R.id.counter);

        list.add(dummy1);
        list.add(dummy2);
        list.add(dummy3);
        list.add(dummy4);

        ticker.setText(list.get(count));
        arraySize = list.size()-1;

        startTicker();
    }

    public void startTicker()
    {
        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
                counter.setText("" +millisUntilFinished / 1000);
            }

            public void onFinish() {
                if(count<arraySize) {
                    count++;
                    tick.setText(list.get(count));
                    startTicker();
                }
                else {
                    count = 0;
                    tick.setText(list.get(count));
                    startTicker();
                }
            }
        }.start();
    }
}
