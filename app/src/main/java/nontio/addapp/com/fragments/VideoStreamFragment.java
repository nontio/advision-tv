package nontio.addapp.com.fragments;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import java.util.ArrayList;

import nontio.addapp.com.tv.R;

/**
 * Created by test on 1/20/2015.
 */
public class VideoStreamFragment extends Fragment{

    ArrayList<String> list = new ArrayList<>();
    VideoView videoView;
    int vidItem = 0;
    Uri uri;

    String url3 = "http://192.168.1.199/uploads/crossange.webm";
    String url2 = "http://192.168.1.199/uploads/wixoss.webm";
    String url1 = "http://192.168.1.199/uploads/shirobako.webm";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.videostream_view, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        list.add(url1);
        list.add(url2);
        list.add(url3);

        videoView = (VideoView) getView().findViewById(R.id.video_view);

        //getVideo();
    }


    public void playVideo(){

        /*MediaController mc = new MediaController(getActivity());
        videoView.setMediaController(mc);*/

        videoView.setVideoURI(uri);

        videoView.requestFocus();
        videoView.start();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (vidItem < list.size() - 1) {
                    vidItem++;
                    getVideo();
                } else {
                    vidItem = 0;
                    getVideo();
                }
            }
        });
    }

    public synchronized void getVideo()
    {
        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>()
        {
            @Override
            public String doInBackground(String... urls)
            {
                uri = Uri.parse(list.get(vidItem));

                return null;
            }

            @Override
            protected void onPostExecute(String result)
            {
                playVideo();
            }
        };
        task.execute();
    }
}
