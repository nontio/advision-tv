package nontio.addapp.com.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import nontio.addapp.com.tv.R;

/**
 * Created by test on 1/20/2015.
 */
public class PictureFragment extends Fragment {

    ArrayList<String> list = new ArrayList<>();
    ImageView imageView;
    int picItem = 0;

    String url1 = "http://192.168.1.199/uploads/kiseju.jpg";
    String url2 = "http://192.168.1.199/uploads/shirobakos.JPG";
    String url3 = "http://192.168.1.199/uploads/shirobako.JPG";
    String url4 = "http://192.168.1.199/uploads/sora%20no%20oto.png";
    String jsonUrl = "http://192.168.1.199/advision/jsonImage.php?time=6%3A00am-12%3A00pm&location=Talisay";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.picture_view, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        imageView = (ImageView) getView().findViewById(R.id.image_view);

        GetImageUrls img = new GetImageUrls(jsonUrl);
        img.execute();
    }

    public void showImage(Bitmap image){
        imageView.setImageBitmap(image);
    }

    public synchronized void getAdImage()
    {
        AsyncTask<Void, Void, Bitmap> task = new AsyncTask<Void, Void, Bitmap>()
        {
            @Override
            public Bitmap doInBackground(Void... params)
            {
                Bitmap adImage = null;
                URL url;
                try
                {
                    url = new URL(list.get(picItem));
                    adImage = BitmapFactory.decodeStream(url.openConnection().getInputStream());

                }catch(MalformedURLException e)
                {
                    e.printStackTrace();
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }

                return adImage;
            }

            @Override
            protected void onPostExecute(Bitmap result)
            {
                showImage(result);
                if(picItem<list.size()-1) {
                    picItem++;
                    startTicker();
                }
                else {
                    picItem = 0;
                    startTicker();
                }
            }


        };
        task.execute();
    }

    public class GetImageUrls extends AsyncTask<String,Void,Boolean>
    {
        JSONObject json;
        JSONArray jsonlist;
        String url;
        String jsonresult;

        public GetImageUrls(String url) {
            this.url = url;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            String temp = "";

            for(int i=0;i<list.size()-1;i++)
                temp.concat(temp +list.get(i));

            Toast.makeText(getActivity(), temp, Toast.LENGTH_SHORT).show();

            //getAdImage();

        }

        @Override
        protected Boolean doInBackground(String... params) {
            try{
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet(url);
                HttpEntity entity;
                HttpResponse response;

                response = client.execute(request);
                entity = response.getEntity();

                jsonresult = EntityUtils.toString(entity);
                jsonlist = new JSONArray(jsonresult);

                for(int i = 0;i<jsonlist.length();i++)
                {
                    json = jsonlist.getJSONObject(i);
                    list.add(json.getString("ImageURL"));
                }

            }catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }
    }

    public void startTicker()
    {
        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
                //counter.setText("" +millisUntilFinished / 1000);
            }

            public void onFinish() {
                    getAdImage();
            }
        }.start();
    }
}
